import React, { Component } from 'react';
import Button from './components/Button';
import Input from './components/Input';
import './App.css';
import ClearButton from './components/ClearButton';

class App extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      displayValue : '',
      previousNum : '',
      evaluated: false
    }
  }
  
// Show input on screen

  addToInput = val => {

    if(this.state.evaluated)
      this.setState({
        displayValue: '',
        evaluated: false
      },()=>this.setState({
         displayValue: this.state.displayValue + val 
      }));

    else this.setState({
        displayValue: this.state.displayValue + val 
      });
  };

  // this is for not input zero at first place
  addZeroInput = val => {
    if(this.state.displayValue !== ''){
      this.setState({ displayValue: this.state.displayValue + val })
    }
  }

 // Handle decimal not input multiple time
  decimalInput = val => {
    if(this.state.displayValue.indexOf(".") === -1){
      this.setState({ displayValue: this.state.displayValue + val })
    }
  }

  // Clear the screen when click on clear button
  clearScreen = () => {
    this.setState({displayValue : ''});
  }

  // To divide

  divide = () => {

    this.setState(prevState=>({
      previousNum: prevState.displayValue,
      displayValue: '',
      operator: 'division'
    }));

  }

  // To multiply

  multiply = () => {

    this.setState(prevState=>({
      previousNum: prevState.displayValue,
      displayValue: '',
      operator: 'multiplication'
    }));

  }

  // To subtract

  subtract = () => {

    this.setState(prevState=>({
      previousNum: prevState.displayValue,
      displayValue: '',
      operator: 'subtraction'
    }));

  }
  
  // To Add

  add = () => {

    this.setState(prevState=>({
      previousNum: prevState.displayValue,
      displayValue: '',
      operator: 'addition'
    }));

  }


  evaluate = () => {

    if(this.state.operator === 'division'){
      this.setState(prevState=>
        ({

          previousNum: prevState.previousNum,
          displayValue : parseInt(this.state.previousNum) / parseInt(prevState.displayValue),
          operator: prevState.operator,
          evaluated: true
        
        })
      
      )
    
    } else if(this.state.operator === 'multiplication') {

      this.setState(prevState=>
        ({

          previousNum: prevState.previousNum,
          displayValue : parseInt(this.state.previousNum) * parseInt(prevState.displayValue),
          operator: prevState.operator
        
        })
      
      )

    } else if(this.state.operator === 'addition') {

      this.setState(prevState=>
        ({

          previousNum: prevState.previousNum,
          displayValue : parseInt(this.state.previousNum) + parseInt(prevState.displayValue),
          operator: prevState.operator
        
        })
      
      )

    } else if(this.state.operator === 'subtraction') {

      this.setState(prevState=>
        ({

          previousNum: prevState.previousNum,
          displayValue : parseInt(this.state.previousNum) - parseInt(prevState.displayValue),
          operator: prevState.operator
        
        })
      
      )      
    }

  }

  render(){

    const displayValue = this.state.displayValue;
    return (
      <div className="App">
        <div className="calc">
          <div className="row">
            <Input val={displayValue}/>
          </div>
          <div className="row">
            <Button handleClick={this.addToInput}>7</Button>
            <Button handleClick={this.addToInput}>8</Button>
            <Button handleClick={this.addToInput}>9</Button>
            <Button handleClick={this.divide}>/</Button>
          </div>
          <div className="row">
            <Button handleClick={this.addToInput}>4</Button>
            <Button handleClick={this.addToInput}>5</Button>
            <Button handleClick={this.addToInput}>6</Button>
            <Button handleClick={this.multiply}>*</Button>
          </div>
          <div className="row">
            <Button handleClick={this.addToInput}>1</Button>
            <Button handleClick={this.addToInput}>2</Button>
            <Button handleClick={this.addToInput}>3</Button>
            <Button handleClick={this.add}>+</Button>
          </div>
          <div className="row">
            <Button handleClick={this.decimalInput}>.</Button>
            <Button handleClick={this.addZeroInput}>0</Button>
            <Button handleClick={this.evaluate}>=</Button>
            <Button handleClick={this.subtract}>-</Button>
          </div>
          <div className="row">
            <ClearButton handleClear={this.clearScreen}>Clear</ClearButton>
          </div>

        </div>
      </div>
    );
  
  }
  
}

export default App;
